#!/bin/bash

#instalo Zenity

if which "zenity" >/dev/null 2>&1; then
	echo "El programa Zenity esta instalado."
else
	echo "El programa Zenity no esta instalado,se procede a instalar"
	sudo apt-get install zenity -y
fi

(
sleep 1
echo "50"
echo "# Comprobando yt-dlp"; sleep 1
if which "yt-dlp" >/dev/null 2>&1; then
	echo "El programa yt-dlp esta instalado."
else
	echo "El programa yt-dlp no esta instalado,se procede a instalar"
	sudo apt-get install yt-dlp -y
fi
sleep 1
echo "90"
echo "# Comprobando python3-brotli"; sleep 1
if which "python3-brotli" >/dev/null 2>&1; then
	echo "El programa python3-brotli esta instalado."
else
	echo "El programa python3-brotli no esta instalado,se procede a instalar"
	sudo apt-get install python3-brotli -y
fi
sleep 1
echo "# Comprobacion finalizada"
echo "100"
) |
zenity --progress \
	--title="Instalacion" \
	--height=100 \
	--width=300 \
	--text="Iniciando el proceso" \
	--percentage=0
if [ "$?" = -1 ] ; then
	zeninty --error \
	--text="Instalacion cancelada."
	exit 0
fi

function aplicacionPrincipal(){
	#captura de datos
	opcion=$(zenity --list --radiolist --height=180 --width=350 --text 'Seleccione el tipo de archivo' --column='seleccion' --column='tipo' TRUE "mp3" FALSE "mp4" )
	texto=$(zenity --entry --height=100 --width=350 --title="Pegue link de Youtube" --text="" )

	#proceso de conversion
	if [[ "$opcion" == "mp3" ]]; then
		yt-dlp --no-warnings -x --audio-format mp3 $texto | zenity --progress --pulsate
        wait
	elif [[ "$opcion" == "mp4" ]]; then
		yt-dlp --no-warnings $texto | zenity --progress --pulsate
        wait
	else
		zenity --error \
		--title= Error \
		--width=250 \
		--text="Alto error amigo."
        exit 0
	fi
	aplicacionPrincipal
}

aplicacionPrincipal



	


